<?php

class Myshipping_Shipping_Model_Carrier
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{

    protected $_code = 'myshipping';

    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->getConfigFlag('active')) {
            return;
        }
        
        $result          = Mage::getModel('shipping/rate_result');
        $method          = Mage::getModel('shipping/rate_result_method');

        $max_weight      = $this->getConfigData('max_weight');
        $weight_products = $request->getPackageWeight();

        if($weight_products < $max_weight){

            $price = '0.0';

            $weight_price    = unserialize($this->getConfigData('weight_price'));

            foreach($weight_price as $item)
            {
                if($weight_products >= $item['weight'])
                {
                    $price = $item['price'];
                }
            }
            
            $method->setCarrier('myshipping');
            $method->setCarrierTitle($this->getConfigData('title'));
    
            $method->setMethod('myshipping');
            $method->setMethodTitle($this->getConfigData('name'));
    
            $method->setPrice($price);
            $method->setCost($price);
    
            $result->append($method);
        }
        return $result;
    }

    public function getAllowedMethods()
    {
        return array('myshipping' => $this->getConfigData('name'));
    }

}